package com.mazapps.template.ui.searchinfo.diffutil

import androidx.recyclerview.widget.DiffUtil
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoEnum

/**
 * @author morad.azzouzi on 06/12/2020.
 */
class SearchInfoDiffCallback(
    private val oldList: List<Pair<SearchInfoEnum, Any>>,
    private val newList: List<Pair<SearchInfoEnum, Any>>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem.second == newItem.second
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem.first == newItem.first && oldItem.second == oldItem.second
    }
}