package com.mazapps.template.ui.searchinfo.viewmodel

import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.data.CallStateEnum.*
import com.mazapps.template.data.DataManager
import com.mazapps.template.data.model.Search
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject

/**
 * @author morad.azzouzi on 11/11/2020.
 */
@ExperimentalStdlibApi
class SearchInfoViewModel(private val dataManager: DataManager) {

    val state: BehaviorSubject<CallStateEnum> = BehaviorSubject.createDefault(IDLE)
    var items: MutableList<Pair<SearchInfoEnum, Any>> = mutableListOf()
    val pendingUpdates = ArrayDeque<MutableList<Pair<SearchInfoEnum, Any>>>()
    var currentList: MutableList<Pair<SearchInfoEnum, Any>> = mutableListOf()

    val itemCount: Int
        get() = items.size
    fun getItemViewTypeAt(position: Int): Int = items[position].first.ordinal
    fun getItemDataAt(position: Int): Any = items[position].second

    fun hasData(): Boolean = items.any { it.first == SearchInfoEnum.SEARCH }

    fun updateCurrentList(list: MutableList<Pair<SearchInfoEnum, Any>>) {
        currentList.clear()
        currentList.addAll(list)
    }

    fun fetchSearchInfo(): Disposable {
        state.onNext(IN_PROGRESS)

        return dataManager
            .fetchSearchInfo("query", "json", "search", KEYWORD)
            .map {
                createRelatedSearchData(it)
            }
            .subscribe(
                {
                    handleFetchSearchInfoSuccess()
                },
                {
                    handleFetchSearchInfoError()
                }
            )
    }

    private fun createRelatedSearchData(search: List<Search>) {
        items = search.map { Pair(SearchInfoEnum.SEARCH, it) }.toMutableList()
        items.add(0, Pair(SearchInfoEnum.HEADER, KEYWORD))
    }

    private fun handleFetchSearchInfoSuccess() {
        state.onNext(SUCCESS)
        state.onNext(IDLE)
    }

    private fun handleFetchSearchInfoError() {
        state.onNext(ERROR)
        state.onNext(IDLE)
    }

    companion object {
        const val KEYWORD = "sasuke"
    }
}