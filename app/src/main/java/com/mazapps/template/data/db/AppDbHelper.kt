package com.mazapps.template.data.db

import com.mazapps.template.data.db.dao.DaoSearch
import com.mazapps.template.data.model.Search
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author morad.azzouzi on 13/11/2020.
 */
@Singleton
class AppDbHelper @Inject constructor(
    private val dao: DaoSearch
) : DbHelper {

    override fun fetchRelatedSearch(): Single<List<Search>> = dao.fetchRelatedSearch()

    override fun insert(search: Search) = dao.insert(search)
}