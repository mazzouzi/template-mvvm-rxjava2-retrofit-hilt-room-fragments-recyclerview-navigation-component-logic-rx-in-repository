package com.mazapps.template.data.db

import com.mazapps.template.data.model.Search
import io.reactivex.Observable
import io.reactivex.Single

/**
 * @author morad.azzouzi on 13/11/2020.
 */
interface DbHelper {

    fun fetchRelatedSearch(): Single<List<Search>>

    fun insert(search: Search)
}