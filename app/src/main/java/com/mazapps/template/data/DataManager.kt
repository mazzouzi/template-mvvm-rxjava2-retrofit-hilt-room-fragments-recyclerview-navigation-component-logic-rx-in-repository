package com.mazapps.template.data

import com.mazapps.template.data.model.Search
import io.reactivex.Observable

/**
 * @author morad.azzouzi on 11/11/2020.
 */
interface DataManager {

    fun fetchSearchInfo(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Observable<List<Search>?>
}