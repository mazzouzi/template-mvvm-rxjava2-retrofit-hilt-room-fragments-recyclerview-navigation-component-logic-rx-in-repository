package com.mazapps.template.data

import com.mazapps.template.data.db.DbHelper
import com.mazapps.template.data.model.Search
import com.mazapps.template.data.network.ApiHelper
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author morad.azzouzi on 01/12/2019.
 */
@Singleton
open class AppDataManager @Inject constructor(
    private val apiHelper: ApiHelper,
    private val dbHelper: DbHelper
) : DataManager {

    override fun fetchSearchInfo(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Observable<List<Search>?> =
        Observable
            .concatArrayEagerDelayError(
                dbHelper
                    .fetchRelatedSearch()
                    .map { searchList: List<Search> ->
                        if (searchList.isNotEmpty()) {
                            // fake data desynchronization between DB and Network API
                            // to show DIFFUTIL capacity
                            val items: MutableList<Search> = searchList.toMutableList()
                            items.removeAt(0)
                            val item = items.removeAt(4)
                            items.removeAt(6)
                            items.add(3, item)
                            items
                        } else {
                            mutableListOf()
                        }
                    }
                    .toObservable()
                ,
                apiHelper
                    .fetchSearchInfo(action, format, list, keyword)
                    .flatMap {
                        Observable.just(it.body()?.query?.search)
                    }
                    .doAfterNext {
                        it?.forEach { search -> dbHelper.insert(search) }
                    }
            )
            .subscribeOn(Schedulers.io())
}