package com.mazapps.template.ui.searchinfo.viewmodel

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mazapps.tabesto.ui.main.viewmodel.fromJson
import com.mazapps.template.TrampolineSchedulerRule
import com.mazapps.template.api.RetrofitApiService
import com.mazapps.template.data.AppDataManager
import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.data.DataManager
import com.mazapps.template.data.db.AppDbHelper
import com.mazapps.template.data.db.DbHelper
import com.mazapps.template.data.db.dao.DaoSearch
import com.mazapps.template.data.model.Search
import com.mazapps.template.data.model.Wikipedia
import com.mazapps.template.data.network.ApiHelper
import com.mazapps.template.data.network.AppApiHelper
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

/**
 * @author morad.azzouzi on 12/11/2020.
 */
@ExperimentalStdlibApi
class SearchInfoViewModelTest {

    @Mock
    var retrofitApiService: RetrofitApiService = Mockito.mock(RetrofitApiService::class.java)
    @Mock
    var dao: DaoSearch = Mockito.mock(DaoSearch::class.java)

    @get:Rule
    val rule = TrampolineSchedulerRule()

    private var stateSubscriber: TestObserver<CallStateEnum> = TestObserver.create()
    private lateinit var viewModel: SearchInfoViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        val apiHelper: ApiHelper = AppApiHelper(retrofitApiService)
        val dbHelper: DbHelper = AppDbHelper(dao)
        val dataManager: DataManager = AppDataManager(apiHelper, dbHelper)
        viewModel = SearchInfoViewModel(dataManager)
    }

    @Test
    fun test_fetchSearchInfo_success() {
        val apiType = object : TypeToken<Wikipedia>() {}.type
        val bodyResponse: Wikipedia = Gson().fromJson(
            "wikipedia.json",
            javaClass.classLoader!!,
            apiType
        )
        val response = Response.success(bodyResponse)

        val daoType = object : TypeToken<List<Search>>() {}.type
        val listOfSearch: List<Search> = Gson().fromJson(
            "related_search.json",
            javaClass.classLoader!!,
            daoType
        )

        // Given
        Mockito
            .`when`(retrofitApiService.fetchSearchInfo(
                anyString(),
                anyString(),
                anyString(),
                anyString())
            )
            .thenReturn(Observable.just(response))
        Mockito
            .`when`(dao.fetchRelatedSearch())
            .thenReturn(Single.just(listOfSearch))

        // When
        viewModel.state.subscribe(stateSubscriber)
        viewModel.fetchSearchInfo()
        viewModel.state.onComplete()

        // Then
        stateSubscriber.awaitTerminalEvent()
        stateSubscriber.assertValues(
            CallStateEnum.IDLE,
            CallStateEnum.IN_PROGRESS,
            CallStateEnum.SUCCESS,
            CallStateEnum.IDLE,
            CallStateEnum.SUCCESS,
            CallStateEnum.IDLE
        )
    }

    @Test
    fun test_fetchSearchInfo_networkApi_error() {
        val daoType = object : TypeToken<List<Search>>() {}.type
        val listOfSearch: List<Search> = Gson().fromJson(
            "related_search.json",
            javaClass.classLoader!!,
            daoType
        )

        // Given
        Mockito
            .`when`(retrofitApiService.fetchSearchInfo(
                anyString(),
                anyString(),
                anyString(),
                anyString())
            )
            .thenReturn(Observable.error(RuntimeException()))
        Mockito
            .`when`(dao.fetchRelatedSearch())
            .thenReturn(Single.just(listOfSearch))

        // When
        viewModel.state.subscribe(stateSubscriber)
        viewModel.fetchSearchInfo()
        viewModel.state.onComplete()

        // Then
        stateSubscriber.awaitTerminalEvent()
        stateSubscriber.assertValues(
            CallStateEnum.IDLE,
            CallStateEnum.IN_PROGRESS,
            CallStateEnum.SUCCESS, // SUCCESS from db api
            CallStateEnum.IDLE,
            CallStateEnum.ERROR, // ERROR (network api) always in last : "concatArrayEagerDelayError"
            CallStateEnum.IDLE
        )
    }

    @Test
    fun test_fetchSearchInfo_database_error() {
        val apiType = object : TypeToken<Wikipedia>() {}.type
        val bodyResponse: Wikipedia = Gson().fromJson(
            "wikipedia.json",
            javaClass.classLoader!!,
            apiType
        )
        val response = Response.success(bodyResponse)

        // Given
        Mockito
            .`when`(retrofitApiService.fetchSearchInfo(
                anyString(),
                anyString(),
                anyString(),
                anyString())
            )
            .thenReturn(Observable.just(response))
        Mockito
            .`when`(dao.fetchRelatedSearch())
            .thenReturn(Single.error(RuntimeException()))

        // When
        viewModel.state.subscribe(stateSubscriber)
        viewModel.fetchSearchInfo()
        viewModel.state.onComplete()

        // Then
        stateSubscriber.awaitTerminalEvent()
        stateSubscriber.assertValues(
            CallStateEnum.IDLE,
            CallStateEnum.IN_PROGRESS,
            CallStateEnum.SUCCESS, // SUCCESS from network api
            CallStateEnum.IDLE,
            CallStateEnum.ERROR, // ERROR (db) always in last : "concatArrayEagerDelayError"
            CallStateEnum.IDLE
        )
    }

    @Test
    fun test_fetchSearchInfo_networkApi_and_database_error() {

        // Given
        Mockito
            .`when`(retrofitApiService.fetchSearchInfo(
                anyString(),
                anyString(),
                anyString(),
                anyString())
            )
            .thenReturn(Observable.error(RuntimeException()))
        Mockito
            .`when`(dao.fetchRelatedSearch())
            .thenReturn(Single.error(RuntimeException()))

        // When
        viewModel.state.subscribe(stateSubscriber)
        viewModel.fetchSearchInfo()
        viewModel.state.onComplete()

        // Then
        stateSubscriber.awaitTerminalEvent()
        stateSubscriber.assertValues(
            CallStateEnum.IDLE,
            CallStateEnum.IN_PROGRESS,
            CallStateEnum.ERROR, // After first error, stream completes
            CallStateEnum.IDLE
        )
    }
}
